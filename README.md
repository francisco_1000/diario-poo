Construir una aplicación web que permita capturar escritos a manera de “diario”. Se deberá guardar
un texto de tamaño indefinido, la fecha y hora en que se realizó el escrito y una calificación de lo
que se escribe que puede ser “bueno”, “malo” o “regular”.
Cada entrada del diario deberá mostrarse en un listado y podrá elegirse una para verla, editarla o
borrarla. La calificación también se podrá cambiar.
Cuando se borre una entrada del diario, ya no será mostrada en el sistema, pero los datos deberán
continuar en la base de datos.