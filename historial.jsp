<%@page contentType="text/html" pageEncoding="UTF-8"
        
import ="java.sql.Connection"        
import ="java.sql.DriverManager"        
import ="java.sql.ResultSet"        
import ="java.sql.Statement"        
import ="java.sql.SQLException"

%>

<%
    
    //Creación de la conexión a la base de datos
    Class.forName("com.mysql.cj.jdbc.Driver");
    Connection conex = (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/diario?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false","root","root");
    Statement sql = conex.createStatement();
    
    //Cadena que ejecuta el comando select para mostrar los valores de la base de datos
    ResultSet data = sql.executeQuery("select * from diario where visibilidad=1");
    
%>



<!Página dedicada a mostrar el historial en forma de tabla>
<!Se añaden las opciones editar y eliminar>
<html>
    
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial</title>
        
    </head>
    
    <body style="background-color:#000000">
        
        <h1 style="color:#ffffff">Historial</h1>
        
        <table style="width:100%; border:1px solid white; border-collapse:collapse ; align-content:flex-start">
            
            <tr>
                
                <th style="color:#ffffff; border:1px solid white">No.</th>
                <th style="color:#ffffff; border:1px solid white">Texto</th>
                <th style="color:#ffffff; border:1px solid white;">Fecha</th>
                <th style="color:#ffffff; border:1px solid white;">Calificación</th>
                
            </tr>
            
            <%
                while (data.next()){
            %>
            
            <tr>
                
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("id")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("texto")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("fecha")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("calificacion")%></td>
                
            </tr>
            
            <%
                } data.close();
            %>
            
        </table><br>
        
        <a style="font-size:20px; color:#ffffff" href="editar.jsp">Editar</a><br><br>
        <a style="font-size:20px; color:#ffffff" href="eliminar.jsp">Eliminar</a><br><br>
        <a style="font-size:20px; color:#ffffff" href="index.html">Regresar</a>
        
    </body>
    
</html>