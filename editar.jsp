<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
        
import ="java.sql.Connection"        
import ="java.sql.DriverManager"        
import ="java.sql.ResultSet"        
import ="java.sql.Statement"        
import ="java.sql.SQLException"

%>


<!Página dedicada a editar una historia>
<html>
    
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Editar</title>
        
    </head>
    
    <body style="background-color:#000000">
        
        <h1 style="color:#ffffff">Editar nota</h1>
        
    </body>
    
    <form>
        
        <label style="color:#ffffff" for="id">Se editará la nota No.</label><br>
        <input style="background-color:#3c3c3c; color:#ffffff" type="text" name="id" >
        <br><br><label style="color:#ffffff" for="Anecdota">Anecdota:</label><br>
        <textarea style="background-color:#3c3c3c; color:#ffffff" name="Anecdota" rows="20" cols="50"></textarea><br><br>
        <label style="color:#ffffff" for="calif">Calificación:</label><br>
        <input type="radio" id="1" name="calif" value="Bueno">
        <label style="color:#ffffff" for="Bueno">Bueno</label><br>
        <input type="radio" id="2" name="calif" value="Regular">
        <label style="color:#ffffff" for="Regular">Regular</label><br>
        <input type="radio" id="3" name="calif" value="Malo">
        <label style="color:#ffffff" for="Malo">Malo</label><br>
        <label style="color:#ffffff" for="Aceptar"></label><br>
        <input type="submit"  value="Aceptar"><br><br>
        <a style="font-size:20px; color:#ffffff" href="index.html">Regresar</a>
        
    </form>
        
</html>


<%
    
    request.setCharacterEncoding("UTF-8");
    
    if (request.getParameter("Anecdota") != null) {
        
        try {
            
            //Creación de la conexión a la base de datos
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conex = (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/diario?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false","root","root");
            Statement sql = conex.createStatement();
            
            //Cadena que ejecuta el comando UPDATE en la base de datos
            //Se actualizan todos los atributos, excepto el ID
            sql.executeUpdate("update diario set texto='"+request.getParameter("Anecdota")+"', calificacion='"+request.getParameter("calif")+"', fecha=current_timestamp where id='"+request.getParameter("id")+"'");
        
        } catch (Exception e){}
    
    }

%>